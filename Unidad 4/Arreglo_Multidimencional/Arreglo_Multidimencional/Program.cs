﻿using System;

namespace Arreglo_Multidimencional
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] Matriz = new int[2,4];
            for(int i=0; i<2;i++)
            {
                for(int j=0;j<4;j++)
                {
                    Console.WriteLine("Ingresa un número");
                    int num = int.Parse(Console.ReadLine());
                    Matriz[i, j] = num;
                    Console.WriteLine("Posición de la matriz");
                    Console.WriteLine(i + "," + j);
                }
            }

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine("Resultado de la matriz");
                    Console.WriteLine(Matriz[i, j]);

                    
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1U4
{
    public partial class CalcularArea : Form
    {
        double area = 0;
        string nom;
        public CalcularArea()
        {
            InitializeComponent();
        }

        private void CalcularArea_Load(object sender, EventArgs e)
        {

        }

        private void Rectangulo_CheckedChanged(object sender, EventArgs e)
        {
            Largo.Clear();
            Altura.Clear();
            Ancho.Clear();
            Radio.Clear();
            Largo.Enabled = true;
            Ancho.Enabled = true;
            Radio.Enabled = false;
           Altura.Enabled = false;
        }

        private void Triangulo_CheckedChanged(object sender, EventArgs e)
        {
            Largo.Clear();
            Altura.Clear();
            Ancho.Clear();
            Radio.Clear();
            Altura.Enabled = true;
            Ancho.Enabled = true;
            Radio.Enabled = false;
            Largo.Enabled = false;
        }

        private void Circulo_CheckedChanged(object sender, EventArgs e)
        {
            Largo.Clear();
            Altura.Clear();
            Ancho.Clear();
            Radio.Clear();
            Radio.Enabled = true;
            Largo.Enabled = false;
            Altura.Enabled = false;
            Ancho.Enabled = false;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Calcular_Click(object sender, EventArgs e)
        {
            if(Rectangulo.Checked==true)
            {
                area = Convert.ToDouble(Largo.Text) * Convert.ToDouble(Ancho.Text);
                nom = "Rectangulo";
            }
            if (Circulo.Checked == true)
            {
                area = Math.PI * Math.Sqrt(Convert.ToDouble(Radio.Text));
                nom = "Circulo";
            }
            if (Triangulo.Checked == true)
            {
                area = (Convert.ToDouble(Ancho.Text) * Convert.ToDouble(Altura.Text)) / 2;
                nom = "Triangulo";
            }
            MessageBox.Show("El Area del "+nom+" es de: "+area);
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Largo_Validating(object sender, CancelEventArgs e)
        {
            if (Largo.Text == "")
            {
                MessageBox.Show("¡Por Favor Rellena la celda!", "¡¡Peligro!!");
                Largo.Focus();
            }
            else
            {
                Ancho.Focus();
            }
        }

        private void Ancho_Validating(object sender, CancelEventArgs e)
        {
            if (Ancho.Text == "")
            {
                MessageBox.Show("¡Por Favor Rellena la celda!", "¡¡Peligro!!");
                Largo.Focus();
                if (Rectangulo.Checked == true)
                {
                    Largo.Focus();
                }
                if (Triangulo.Checked == true)
                {
                    Altura.Focus();
                }
            }

        }

        private void Radio_Validating(object sender, CancelEventArgs e)
        {
            if (Radio.Text == "")
            {
                MessageBox.Show("¡Por Favor Rellena la celda!", "¡¡Peligro!!");
                Largo.Focus();
            }
        }

        private void Altura_Validating(object sender, CancelEventArgs e)
        {
            if (Altura.Text == "")
            {
                MessageBox.Show("¡Por Favor Rellena la celda!", "¡¡Peligro!!");
                Largo.Focus();
            }
            else
            {
                Ancho.Focus();
            }
        }
    }
}

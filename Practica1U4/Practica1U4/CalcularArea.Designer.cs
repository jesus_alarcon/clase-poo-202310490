﻿
namespace Practica1U4
{
    partial class CalcularArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Rectangulo = new System.Windows.Forms.RadioButton();
            this.Triangulo = new System.Windows.Forms.RadioButton();
            this.Circulo = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.Largo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Ancho = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Radio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Altura = new System.Windows.Forms.TextBox();
            this.Calcular = new System.Windows.Forms.Button();
            this.Salir = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Rectangulo
            // 
            this.Rectangulo.AutoSize = true;
            this.Rectangulo.Location = new System.Drawing.Point(82, 51);
            this.Rectangulo.Name = "Rectangulo";
            this.Rectangulo.Size = new System.Drawing.Size(80, 17);
            this.Rectangulo.TabIndex = 0;
            this.Rectangulo.TabStop = true;
            this.Rectangulo.Text = "Rectangulo";
            this.Rectangulo.UseVisualStyleBackColor = true;
            this.Rectangulo.CheckedChanged += new System.EventHandler(this.Rectangulo_CheckedChanged);
            // 
            // Triangulo
            // 
            this.Triangulo.AutoSize = true;
            this.Triangulo.Location = new System.Drawing.Point(82, 75);
            this.Triangulo.Name = "Triangulo";
            this.Triangulo.Size = new System.Drawing.Size(69, 17);
            this.Triangulo.TabIndex = 1;
            this.Triangulo.TabStop = true;
            this.Triangulo.Text = "Triangulo";
            this.Triangulo.UseVisualStyleBackColor = true;
            this.Triangulo.CheckedChanged += new System.EventHandler(this.Triangulo_CheckedChanged);
            // 
            // Circulo
            // 
            this.Circulo.AutoSize = true;
            this.Circulo.Location = new System.Drawing.Point(82, 99);
            this.Circulo.Name = "Circulo";
            this.Circulo.Size = new System.Drawing.Size(57, 17);
            this.Circulo.TabIndex = 2;
            this.Circulo.TabStop = true;
            this.Circulo.Text = "Circulo";
            this.Circulo.UseVisualStyleBackColor = true;
            this.Circulo.CheckedChanged += new System.EventHandler(this.Circulo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Largo";
            // 
            // Largo
            // 
            this.Largo.Location = new System.Drawing.Point(63, 19);
            this.Largo.Name = "Largo";
            this.Largo.Size = new System.Drawing.Size(100, 20);
            this.Largo.TabIndex = 4;
            this.Largo.Validating += new System.ComponentModel.CancelEventHandler(this.Largo_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ancho";
            // 
            // Ancho
            // 
            this.Ancho.Location = new System.Drawing.Point(63, 45);
            this.Ancho.Name = "Ancho";
            this.Ancho.Size = new System.Drawing.Size(100, 20);
            this.Ancho.TabIndex = 4;
            this.Ancho.Validating += new System.ComponentModel.CancelEventHandler(this.Ancho_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Radio";
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(63, 71);
            this.Radio.Name = "Radio";
            this.Radio.Size = new System.Drawing.Size(100, 20);
            this.Radio.TabIndex = 4;
            this.Radio.Validating += new System.ComponentModel.CancelEventHandler(this.Radio_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Altura";
            // 
            // Altura
            // 
            this.Altura.Location = new System.Drawing.Point(63, 97);
            this.Altura.Name = "Altura";
            this.Altura.Size = new System.Drawing.Size(100, 20);
            this.Altura.TabIndex = 4;
            this.Altura.Validating += new System.ComponentModel.CancelEventHandler(this.Altura_Validating);
            // 
            // Calcular
            // 
            this.Calcular.Location = new System.Drawing.Point(221, 75);
            this.Calcular.Name = "Calcular";
            this.Calcular.Size = new System.Drawing.Size(75, 23);
            this.Calcular.TabIndex = 5;
            this.Calcular.Text = "Calcular";
            this.Calcular.UseVisualStyleBackColor = true;
            this.Calcular.Click += new System.EventHandler(this.Calcular_Click);
            // 
            // Salir
            // 
            this.Salir.Location = new System.Drawing.Point(221, 104);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(75, 23);
            this.Salir.TabIndex = 5;
            this.Salir.Text = "Salir";
            this.Salir.UseVisualStyleBackColor = true;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(61, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 29);
            this.label5.TabIndex = 3;
            this.label5.Text = "Calcular el area de:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Ancho);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Largo);
            this.groupBox1.Controls.Add(this.Altura);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Radio);
            this.groupBox1.Location = new System.Drawing.Point(24, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 129);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // CalcularArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 292);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Salir);
            this.Controls.Add(this.Calcular);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Circulo);
            this.Controls.Add(this.Triangulo);
            this.Controls.Add(this.Rectangulo);
            this.Name = "CalcularArea";
            this.Text = "CalcularArea";
            this.Load += new System.EventHandler(this.CalcularArea_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Rectangulo;
        private System.Windows.Forms.RadioButton Triangulo;
        private System.Windows.Forms.RadioButton Circulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Largo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Ancho;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Radio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Altura;
        private System.Windows.Forms.Button Calcular;
        private System.Windows.Forms.Button Salir;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
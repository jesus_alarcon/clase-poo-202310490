﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace metodo2._1
{
    public partial class Form1 : Form
    {
        public int turn = 0;
        public int canal = 0;
        public int volum = 0;
        tv estados = new tv();
        public Form1()
        {
            InitializeComponent();
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();
            TV1.settamanio(30);
            int tamanioTV1 = TV1.gettamanio();
            TV1.setvolumen(50);
            int volumenTV1 = TV1.getvolumen();
            MessageBox.Show("El tamaño de la TV es " + tamanioTV1 + " Pulgadas\n"+"El Volumen de la TV es "+volumenTV1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            series.Visible = false;
        }

        private void TurnOn_Click(object sender, EventArgs e)
        {

            if (turn == 0)
            {
                if (estados.getmute()== 1)
                {
                    estados.setmute(0);
                    mutelab.Visible = false;
                }
                turn = 1;
                series.Visible = true;
                switch (canal)
                {
                    case 0:
                        {
                            samwashere.Visible = true;
                            break;
                        }
                    case 1:
                        {
                            breakingbad.Visible = true;
                            break;
                        }
                    case 2:
                        {
                            malcomcap.Visible = true;
                            break;
                        }
                    case 3:
                        {
                            sexeducation.Visible = true;
                            break;
                        }
                    case 4:
                        {
                            scissorseven.Visible = true;
                            break;
                        }
                    case 5:
                        {
                            donniedarko.Visible = true;
                            break;
                        }
                    case 6:
                        {
                            gotham.Visible = true;
                            break;
                        }
                    case 7:
                        {
                            oficcecap.Visible = true;
                            break;
                        }
                    case 8:
                        {
                            flash.Visible = true;
                            break;
                        }
                    case 9:
                        {
                            standelosbesos.Visible = true;
                            break;
                        }
                }
            }
            else
            {
                if (turn == 1)
                {
                    turn = 0;
                    series.Visible = false;
                }
            }
        }

        private void Mute_Click(object sender, EventArgs e)
        {
            
            if (estados.getmute() == 0)
            {
                estados.setmute(1);
                mutelab.Visible = true;
            }
            else
            {
                    estados.setmute(0);
                    mutelab.Visible = false;
                
            }
        }
        private void chnlpl_Click_1(object sender, EventArgs e)
        {
            if (turn == 1)
            {
                canal = canal + 1;
            }
            if (canal >= 0 && canal <= 10)
            {
                switch (canal)
                {
                    case 0:
                        {
                            standelosbesos.Visible = false;
                            samwashere.Visible = true;
                            breakingbad.Visible = false;
                            break;
                        }
                    case 1:
                        {
                            samwashere.Visible = false;
                            breakingbad.Visible = true;
                            malcomcap.Visible = false;
                            break;
                        }
                    case 2:
                        {
                            breakingbad.Visible = false;
                            malcomcap.Visible = true;
                            sexeducation.Visible = false;
                            break;
                        }
                    case 3:
                        {
                            malcomcap.Visible = false;
                            sexeducation.Visible = true;
                            scissorseven.Visible = false;
                            break;
                        }
                    case 4:
                        {
                            sexeducation.Visible = false;
                            scissorseven.Visible = true;
                            donniedarko.Visible = false;
                            break;
                        }
                    case 5:
                        {
                            scissorseven.Visible = false;
                            donniedarko.Visible = true;
                            gotham.Visible = false;
                            break;
                        }
                    case 6:
                        {
                            donniedarko.Visible = false;
                            gotham.Visible = true;
                            oficcecap.Visible = false;
                            break;
                        }
                    case 7:
                        {
                            gotham.Visible = false;
                            oficcecap.Visible = true;
                            flash.Visible = false;
                            break;
                        }
                    case 8:
                        {
                            oficcecap.Visible = false;
                            flash.Visible = true;
                            standelosbesos.Visible = false;
                            break;
                        }
                    case 9:
                        {
                            flash.Visible = false;
                            standelosbesos.Visible = true;
                            samwashere.Visible = false;
                            break;
                        }
                    default:
                        standelosbesos.Visible = false;
                        samwashere.Visible = true;
                        breakingbad.Visible = false;
                        canal = 0;
                        break;

                }
                

            }
        }

        private void chnless_Click(object sender, EventArgs e)
        {
            if (turn == 1)
            {
                canal = canal - 1;
            }
            if (canal >= -1 && canal <= 9)
            {
                switch (canal)
                {
                    case 0:
                        {
                            standelosbesos.Visible = false;
                            samwashere.Visible = true;
                            breakingbad.Visible = false;
                            break;
                        }
                    case 1:
                        {
                            samwashere.Visible = false;
                            breakingbad.Visible = true;
                            malcomcap.Visible = false;
                            break;
                        }
                    case 2:
                        {
                            breakingbad.Visible = false;
                            malcomcap.Visible = true;
                            sexeducation.Visible = false;
                            break;
                        }
                    case 3:
                        {
                            malcomcap.Visible = false;
                            sexeducation.Visible = true;
                            scissorseven.Visible = false;
                            break;
                        }
                    case 4:
                        {
                            sexeducation.Visible = false;
                            scissorseven.Visible = true;
                            donniedarko.Visible = false;
                            break;
                        }
                    case 5:
                        {
                            scissorseven.Visible = false;
                            donniedarko.Visible = true;
                            gotham.Visible = false;
                            break;
                        }
                    case 6:
                        {
                            donniedarko.Visible = false;
                            gotham.Visible = true;
                            oficcecap.Visible = false;
                            break;
                        }
                    case 7:
                        {
                            gotham.Visible = false;
                            oficcecap.Visible = true;
                            flash.Visible = false;
                            break;
                        }
                    case 8:
                        {
                            oficcecap.Visible = false;
                            flash.Visible = true;
                            standelosbesos.Visible = false;
                            break;
                        }
                    case 9:
                        {
                            flash.Visible = false;
                            standelosbesos.Visible = true;
                            samwashere.Visible = false;
                            break;
                        }
                    default:
                        flash.Visible = false;
                        standelosbesos.Visible = true;
                        samwashere.Visible = false;
                        canal = 9;
                        break;

                }
                

            }
        }

        private void volplus_Click(object sender, EventArgs e)
        {
            if (volum >= 0 && volum <= 90)
            {
                volum += 10;
                timer1.Enabled = true;
                timer1.Start();
                if (estados.getmute() == 1)
                {
                    estados.setmute(0);
                    mutelab.Visible = false;
                }
            }
            else
                volum = 100;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            volumen.Text = volum.ToString();
            volumen.Visible = true;
            timer1.Enabled = false;
            timer2.Enabled = true;
            timer2.Start();
        }

        private void volumless_Click(object sender, EventArgs e)
        {
            if (volum >= 10 && volum <= 100)
            {
                if (estados.getmute() == 1)
                {
                    estados.setmute(0);
                    mutelab.Visible = false;
                }
                volum -= 10;
                timer1.Enabled = true;
                timer1.Start();
            }
            else
                volum = 0;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            volumen.Visible = false;
            timer1.Stop();
            timer2.Stop();
        }
    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace metodo2._1.Properties {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("metodo2._1.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap AAAABQJIhL8UhGD5TBu0jry_NC7BmrsqdG4WauBFuKuDq_PC2uDQ6jWhLfGTnSVZtyUpZn1cZFUUf_PviGFpMcl85m_YljVa_1200x675 {
            get {
                object obj = ResourceManager.GetObject("AAAABQJIhL8UhGD5TBu0jry_NC7BmrsqdG4WauBFuKuDq_PC2uDQ6jWhLfGTnSVZtyUpZn1cZFUUf_Pvi" +
                        "GFpMcl85m_YljVa-1200x675", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap botonapagado {
            get {
                object obj = ResourceManager.GetObject("botonapagado", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap botonapagado1 {
            get {
                object obj = ResourceManager.GetObject("botonapagado1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap breaking_bad {
            get {
                object obj = ResourceManager.GetObject("breaking_bad", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap donniedarko {
            get {
                object obj = ResourceManager.GetObject("donniedarko", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap kisspng_remote_controls_universal_remote_set_top_box_telev_control_5ad95d9607bca7_7168784415241947100317 {
            get {
                object obj = ResourceManager.GetObject("kisspng-remote-controls-universal-remote-set-top-box-telev-control-5ad95d9607bca7" +
                        ".7168784415241947100317", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap maxresdefault {
            get {
                object obj = ResourceManager.GetObject("maxresdefault", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap maxresdefault__1_ {
            get {
                object obj = ResourceManager.GetObject("maxresdefault (1)", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap maxresdefault__2_ {
            get {
                object obj = ResourceManager.GetObject("maxresdefault (2)", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap MV5BOWI0NzZjZjQtYzEzZC00ZWJiLTg1NTMtNmZhMmYyOGYwYWQ1XkEyXkFqcGdeQXVyNjA0OTQyODE___V1_ {
            get {
                object obj = ResourceManager.GetObject("MV5BOWI0NzZjZjQtYzEzZC00ZWJiLTg1NTMtNmZhMmYyOGYwYWQ1XkEyXkFqcGdeQXVyNjA0OTQyODE@." +
                        "_V1_", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SALACONTROLTV {
            get {
                object obj = ResourceManager.GetObject("SALACONTROLTV", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Sam_Was_Here_2016_movie_Christophe_Deroo_1 {
            get {
                object obj = ResourceManager.GetObject("Sam-Was-Here-2016-movie-Christophe-Deroo-1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sex_education_netflix_jan19_jon_hall_netflix_1 {
            get {
                object obj = ResourceManager.GetObject("sex-education-netflix-jan19-jon-hall_netflix-1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap TheFight {
            get {
                object obj = ResourceManager.GetObject("TheFight", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap tv_pared_gabinete_sala_estar_41470_17 {
            get {
                object obj = ResourceManager.GetObject("tv-pared-gabinete-sala-estar_41470-17", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}

﻿
namespace metodo2._1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.malcomcap = new System.Windows.Forms.PictureBox();
            this.oficcecap = new System.Windows.Forms.PictureBox();
            this.TurnOn = new System.Windows.Forms.Button();
            this.Mute = new System.Windows.Forms.Button();
            this.donniedarko = new System.Windows.Forms.PictureBox();
            this.breakingbad = new System.Windows.Forms.PictureBox();
            this.scissorseven = new System.Windows.Forms.PictureBox();
            this.standelosbesos = new System.Windows.Forms.PictureBox();
            this.sexeducation = new System.Windows.Forms.PictureBox();
            this.flash = new System.Windows.Forms.PictureBox();
            this.gotham = new System.Windows.Forms.PictureBox();
            this.series = new System.Windows.Forms.GroupBox();
            this.mutelab = new System.Windows.Forms.Label();
            this.samwashere = new System.Windows.Forms.PictureBox();
            this.chnlpl = new System.Windows.Forms.Button();
            this.chnless = new System.Windows.Forms.Button();
            this.volplus = new System.Windows.Forms.Button();
            this.volumless = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.volumen = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.malcomcap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oficcecap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donniedarko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breakingbad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorseven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standelosbesos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sexeducation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gotham)).BeginInit();
            this.series.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.samwashere)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(631, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 23);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // malcomcap
            // 
            this.malcomcap.BackgroundImage = global::metodo2._1.Properties.Resources.MV5BOWI0NzZjZjQtYzEzZC00ZWJiLTg1NTMtNmZhMmYyOGYwYWQ1XkEyXkFqcGdeQXVyNjA0OTQyODE___V1_;
            this.malcomcap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.malcomcap.Location = new System.Drawing.Point(18, 22);
            this.malcomcap.Name = "malcomcap";
            this.malcomcap.Size = new System.Drawing.Size(277, 170);
            this.malcomcap.TabIndex = 2;
            this.malcomcap.TabStop = false;
            this.malcomcap.Visible = false;
            // 
            // oficcecap
            // 
            this.oficcecap.BackgroundImage = global::metodo2._1.Properties.Resources.TheFight;
            this.oficcecap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.oficcecap.Location = new System.Drawing.Point(18, 22);
            this.oficcecap.Name = "oficcecap";
            this.oficcecap.Size = new System.Drawing.Size(277, 170);
            this.oficcecap.TabIndex = 3;
            this.oficcecap.TabStop = false;
            this.oficcecap.Visible = false;
            // 
            // TurnOn
            // 
            this.TurnOn.BackColor = System.Drawing.Color.Transparent;
            this.TurnOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.TurnOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TurnOn.FlatAppearance.BorderSize = 0;
            this.TurnOn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TurnOn.Location = new System.Drawing.Point(96, 103);
            this.TurnOn.Name = "TurnOn";
            this.TurnOn.Size = new System.Drawing.Size(21, 21);
            this.TurnOn.TabIndex = 4;
            this.TurnOn.UseVisualStyleBackColor = false;
            this.TurnOn.Click += new System.EventHandler(this.TurnOn_Click);
            // 
            // Mute
            // 
            this.Mute.BackColor = System.Drawing.Color.Transparent;
            this.Mute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Mute.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mute.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Mute.FlatAppearance.BorderSize = 0;
            this.Mute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Mute.Location = new System.Drawing.Point(163, 103);
            this.Mute.Name = "Mute";
            this.Mute.Size = new System.Drawing.Size(22, 21);
            this.Mute.TabIndex = 4;
            this.Mute.UseVisualStyleBackColor = false;
            this.Mute.Click += new System.EventHandler(this.Mute_Click);
            // 
            // donniedarko
            // 
            this.donniedarko.BackgroundImage = global::metodo2._1.Properties.Resources.donniedarko;
            this.donniedarko.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.donniedarko.Location = new System.Drawing.Point(18, 22);
            this.donniedarko.Name = "donniedarko";
            this.donniedarko.Size = new System.Drawing.Size(277, 170);
            this.donniedarko.TabIndex = 3;
            this.donniedarko.TabStop = false;
            this.donniedarko.Visible = false;
            // 
            // breakingbad
            // 
            this.breakingbad.BackgroundImage = global::metodo2._1.Properties.Resources.breaking_bad;
            this.breakingbad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.breakingbad.Location = new System.Drawing.Point(18, 22);
            this.breakingbad.Name = "breakingbad";
            this.breakingbad.Size = new System.Drawing.Size(277, 170);
            this.breakingbad.TabIndex = 3;
            this.breakingbad.TabStop = false;
            this.breakingbad.Visible = false;
            // 
            // scissorseven
            // 
            this.scissorseven.BackgroundImage = global::metodo2._1.Properties.Resources.AAAABQJIhL8UhGD5TBu0jry_NC7BmrsqdG4WauBFuKuDq_PC2uDQ6jWhLfGTnSVZtyUpZn1cZFUUf_PviGFpMcl85m_YljVa_1200x675;
            this.scissorseven.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.scissorseven.Location = new System.Drawing.Point(18, 22);
            this.scissorseven.Name = "scissorseven";
            this.scissorseven.Size = new System.Drawing.Size(277, 170);
            this.scissorseven.TabIndex = 3;
            this.scissorseven.TabStop = false;
            this.scissorseven.Visible = false;
            // 
            // standelosbesos
            // 
            this.standelosbesos.BackgroundImage = global::metodo2._1.Properties.Resources.maxresdefault;
            this.standelosbesos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.standelosbesos.Location = new System.Drawing.Point(18, 22);
            this.standelosbesos.Name = "standelosbesos";
            this.standelosbesos.Size = new System.Drawing.Size(277, 170);
            this.standelosbesos.TabIndex = 3;
            this.standelosbesos.TabStop = false;
            this.standelosbesos.Visible = false;
            // 
            // sexeducation
            // 
            this.sexeducation.BackgroundImage = global::metodo2._1.Properties.Resources.sex_education_netflix_jan19_jon_hall_netflix_1;
            this.sexeducation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sexeducation.Location = new System.Drawing.Point(18, 22);
            this.sexeducation.Name = "sexeducation";
            this.sexeducation.Size = new System.Drawing.Size(277, 170);
            this.sexeducation.TabIndex = 3;
            this.sexeducation.TabStop = false;
            this.sexeducation.Visible = false;
            // 
            // flash
            // 
            this.flash.BackgroundImage = global::metodo2._1.Properties.Resources.maxresdefault__1_;
            this.flash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flash.Location = new System.Drawing.Point(18, 22);
            this.flash.Name = "flash";
            this.flash.Size = new System.Drawing.Size(277, 170);
            this.flash.TabIndex = 3;
            this.flash.TabStop = false;
            this.flash.Visible = false;
            // 
            // gotham
            // 
            this.gotham.BackgroundImage = global::metodo2._1.Properties.Resources.maxresdefault__2_;
            this.gotham.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gotham.Location = new System.Drawing.Point(18, 22);
            this.gotham.Name = "gotham";
            this.gotham.Size = new System.Drawing.Size(277, 170);
            this.gotham.TabIndex = 3;
            this.gotham.TabStop = false;
            this.gotham.Visible = false;
            // 
            // series
            // 
            this.series.BackColor = System.Drawing.Color.Transparent;
            this.series.Controls.Add(this.volumen);
            this.series.Controls.Add(this.label1);
            this.series.Controls.Add(this.mutelab);
            this.series.Controls.Add(this.samwashere);
            this.series.Controls.Add(this.gotham);
            this.series.Controls.Add(this.malcomcap);
            this.series.Controls.Add(this.oficcecap);
            this.series.Controls.Add(this.flash);
            this.series.Controls.Add(this.donniedarko);
            this.series.Controls.Add(this.sexeducation);
            this.series.Controls.Add(this.breakingbad);
            this.series.Controls.Add(this.standelosbesos);
            this.series.Controls.Add(this.scissorseven);
            this.series.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.series.Location = new System.Drawing.Point(246, 64);
            this.series.Name = "series";
            this.series.Size = new System.Drawing.Size(307, 209);
            this.series.TabIndex = 5;
            this.series.TabStop = false;
            // 
            // mutelab
            // 
            this.mutelab.AutoSize = true;
            this.mutelab.Font = new System.Drawing.Font("Yu Gothic UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mutelab.ForeColor = System.Drawing.Color.Gainsboro;
            this.mutelab.Location = new System.Drawing.Point(32, 34);
            this.mutelab.Name = "mutelab";
            this.mutelab.Size = new System.Drawing.Size(43, 17);
            this.mutelab.TabIndex = 4;
            this.mutelab.Text = "MUTE";
            this.mutelab.Visible = false;
            // 
            // samwashere
            // 
            this.samwashere.BackgroundImage = global::metodo2._1.Properties.Resources.Sam_Was_Here_2016_movie_Christophe_Deroo_1;
            this.samwashere.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.samwashere.Location = new System.Drawing.Point(18, 22);
            this.samwashere.Name = "samwashere";
            this.samwashere.Size = new System.Drawing.Size(277, 170);
            this.samwashere.TabIndex = 3;
            this.samwashere.TabStop = false;
            this.samwashere.Visible = false;
            // 
            // chnlpl
            // 
            this.chnlpl.BackColor = System.Drawing.Color.Transparent;
            this.chnlpl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chnlpl.ForeColor = System.Drawing.Color.Transparent;
            this.chnlpl.Location = new System.Drawing.Point(118, 282);
            this.chnlpl.Name = "chnlpl";
            this.chnlpl.Size = new System.Drawing.Size(41, 23);
            this.chnlpl.TabIndex = 6;
            this.chnlpl.UseVisualStyleBackColor = false;
            this.chnlpl.Click += new System.EventHandler(this.chnlpl_Click_1);
            // 
            // chnless
            // 
            this.chnless.BackColor = System.Drawing.Color.Transparent;
            this.chnless.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chnless.ForeColor = System.Drawing.Color.Transparent;
            this.chnless.Location = new System.Drawing.Point(119, 332);
            this.chnless.Name = "chnless";
            this.chnless.Size = new System.Drawing.Size(41, 20);
            this.chnless.TabIndex = 6;
            this.chnless.UseVisualStyleBackColor = false;
            this.chnless.Click += new System.EventHandler(this.chnless_Click);
            // 
            // volplus
            // 
            this.volplus.BackColor = System.Drawing.Color.Transparent;
            this.volplus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.volplus.Location = new System.Drawing.Point(159, 291);
            this.volplus.Name = "volplus";
            this.volplus.Size = new System.Drawing.Size(22, 54);
            this.volplus.TabIndex = 7;
            this.volplus.UseVisualStyleBackColor = false;
            this.volplus.Click += new System.EventHandler(this.volplus_Click);
            // 
            // volumless
            // 
            this.volumless.BackColor = System.Drawing.Color.Transparent;
            this.volumless.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.volumless.Location = new System.Drawing.Point(96, 291);
            this.volumless.Name = "volumless";
            this.volumless.Size = new System.Drawing.Size(22, 54);
            this.volumless.TabIndex = 7;
            this.volumless.UseVisualStyleBackColor = false;
            this.volumless.Click += new System.EventHandler(this.volumless_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(242, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // volumen
            // 
            this.volumen.AutoSize = true;
            this.volumen.BackColor = System.Drawing.Color.DimGray;
            this.volumen.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.volumen.Location = new System.Drawing.Point(234, 34);
            this.volumen.Name = "volumen";
            this.volumen.Size = new System.Drawing.Size(35, 13);
            this.volumen.TabIndex = 6;
            this.volumen.Text = "label2";
            this.volumen.Visible = false;
            // 
            // timer2
            // 
            this.timer2.Interval = 300;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::metodo2._1.Properties.Resources.SALACONTROLTV;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.volumless);
            this.Controls.Add(this.volplus);
            this.Controls.Add(this.chnless);
            this.Controls.Add(this.chnlpl);
            this.Controls.Add(this.Mute);
            this.Controls.Add(this.TurnOn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.series);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.malcomcap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oficcecap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donniedarko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breakingbad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorseven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standelosbesos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sexeducation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gotham)).EndInit();
            this.series.ResumeLayout(false);
            this.series.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.samwashere)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox malcomcap;
        private System.Windows.Forms.PictureBox oficcecap;
        private System.Windows.Forms.Button TurnOn;
        private System.Windows.Forms.Button Mute;
        private System.Windows.Forms.PictureBox donniedarko;
        private System.Windows.Forms.PictureBox breakingbad;
        private System.Windows.Forms.PictureBox scissorseven;
        private System.Windows.Forms.PictureBox standelosbesos;
        private System.Windows.Forms.PictureBox sexeducation;
        private System.Windows.Forms.PictureBox flash;
        private System.Windows.Forms.PictureBox gotham;
        private System.Windows.Forms.GroupBox series;
        private System.Windows.Forms.Label mutelab;
        private System.Windows.Forms.PictureBox samwashere;
        private System.Windows.Forms.Button chnlpl;
        private System.Windows.Forms.Button chnless;
        private System.Windows.Forms.Button volplus;
        private System.Windows.Forms.Button volumless;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label volumen;
        private System.Windows.Forms.Timer timer2;
    }
}


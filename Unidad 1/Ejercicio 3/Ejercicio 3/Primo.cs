﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Primo
    {
        private int num = 0;
        
        public void ImprimePrimo()
        {
            num = int.Parse(Console.ReadLine());
            
            if (num %2 == 0)
            {
                Console.WriteLine("El numero {0} no es primo", num);
            }
            else
            {
                Console.WriteLine("El número es primo");
            }

        }

        ~Primo()
        {
            num = 0;
        }
    }
}

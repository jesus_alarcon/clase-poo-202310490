﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Operaciones
{
    class Operacion
    {
        private int a, b, c;

        public void Suma()
        {
            Console.WriteLine("Ingresa el valor de a");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingresa el valor de b");
            b = int.Parse(Console.ReadLine());

            c = a + b;

            Console.WriteLine("La suma de {0} + {1} es: {2} ",a,b,c);
            
        }
    }
}

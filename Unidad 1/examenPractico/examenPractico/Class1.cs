﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examenPractico
{
    class compu
    {
        public int ram = 0;
        public int memoria = 0;
        public string procesador = "";
        public int vram = 0;
        public void setram(int ram)
        {
            this.ram = ram;
        }
        public int getram()
        {
            return this.ram;
        }
        public void setmemoria(int memoria)
        {
            this.memoria = memoria;
        }
        public int getmemoria()
        {
            return this.memoria;
        }
        public void setprocesador(string procesador)
        {
            this.procesador = procesador;
        }
        public string getprocesador()
        {
            return this.procesador;
        }
        public void setvram(int vram)
        {
            this.vram = vram;
        }
        public int getvram()
        {
            return this.vram;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examenPractico
{

    public partial class Form1 : Form
    {
        compu computadora = new compu();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            computadora.setprocesador(listBox1.SelectedItem.ToString());
            string procesador = computadora.getprocesador();
            if (listBox1.SelectedIndex == 0)
            {
                label1.Visible = true;
                computadora.setram(2);
                int ram1 = computadora.getram();
                computadora.setmemoria(500);
                int memoria1 = computadora.getmemoria();
                computadora.setvram(1);
                int vram1 = computadora.getvram();
                label1.Text = "En el procesador " + procesador + "\nLa ram es de: " + ram1 + "\nLa Memoria es de: " + memoria1 + "\nLa memoria de video es de:" + vram1;
            }
            if (listBox1.SelectedIndex == 1)
            {
                label1.Visible = true;
                computadora.setram(4);
                int ram1 = computadora.getram();
                computadora.setmemoria(1000);
                int memoria1 = computadora.getmemoria();
                computadora.setvram(2);
                int vram1 = computadora.getvram();
                label1.Text = "En el procesador " + procesador + "\nLa ram es de: " + ram1 + "\nLa Memoria es de: " + memoria1 + "\nLa memoria de video es de:" + vram1;
            }
            if (listBox1.SelectedIndex == 2)
            {
                label1.Visible = true;
                computadora.setram(8);
                int ram1 = computadora.getram();
                computadora.setmemoria(2000);
                int memoria1 = computadora.getmemoria();
                computadora.setvram(4);
                int vram1 = computadora.getvram();
                label1.Text = "En el procesador " + procesador + "\nLa ram es de: " + ram1 + "\nLa Memoria es de: " + memoria1 + "\nLa memoria de video es de:" + vram1;
            }
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

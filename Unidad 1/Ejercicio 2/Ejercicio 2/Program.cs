﻿using System;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Programa que genera 10 números aleatorios");
            Aleatorio objRan = new Aleatorio(10);
            objRan.ImprimeAleatorio();
        }
    }
}

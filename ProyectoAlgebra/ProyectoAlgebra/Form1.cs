﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoAlgebra
{

    public partial class Form1 : Form
    {
        int xm1 = 0, ym1 = 0;

        private void textBox28_TextChanged(object sender, EventArgs e)
        {
            if (textBox28.Text == "")
            {
                textBox28.Text = "0";
            }
            if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 3)
            {
                M1TB1.Enabled = true;
                M1TB2.Enabled = true;
                M1TB3.Enabled = true;
                M1TB4.Enabled = true;
                M1TB5.Enabled = true;
                M1TB6.Enabled = true;
                M1TB7.Enabled = true;
                M1TB8.Enabled = true;
                M1TB9.Enabled = true;
            }
            else
            {
                if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 3)

                {
                    M1TB1.Enabled = true;
                    M1TB2.Enabled = true;
                    M1TB3.Enabled = true;
                    M1TB4.Enabled = true;
                    M1TB5.Enabled = true;
                    M1TB6.Enabled = true;
                    M1TB7.Enabled = false;
                    M1TB8.Enabled = false;
                    M1TB9.Enabled = false;
                }

                else
                {
                    if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 3)
                    {
                        M1TB1.Enabled = true;
                        M1TB2.Enabled = true;
                        M1TB3.Enabled = true;
                        M1TB4.Enabled = false;
                        M1TB5.Enabled = false;
                        M1TB6.Enabled = false;
                        M1TB7.Enabled = false;
                        M1TB8.Enabled = false;
                        M1TB9.Enabled = false;
                    }
                    else
                    {
                        if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 2)
                        {
                            M1TB1.Enabled = true;
                            M1TB2.Enabled = true;
                            M1TB3.Enabled = false;
                            M1TB4.Enabled = true;
                            M1TB5.Enabled = true;
                            M1TB6.Enabled = false;
                            M1TB7.Enabled = true;
                            M1TB8.Enabled = true;
                            M1TB9.Enabled = false;
                        }
                        else
                        {
                            if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 2)
                            {
                                M1TB1.Enabled = true;
                                M1TB2.Enabled = true;
                                M1TB3.Enabled = false;
                                M1TB4.Enabled = true;
                                M1TB5.Enabled = true;
                                M1TB6.Enabled = false;
                                M1TB7.Enabled = false;
                                M1TB8.Enabled = false;
                                M1TB9.Enabled = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 2)
                                {
                                    M1TB1.Enabled = true;
                                    M1TB2.Enabled = true;
                                    M1TB3.Enabled = false;
                                    M1TB4.Enabled = false;
                                    M1TB5.Enabled = false;
                                    M1TB6.Enabled = false;
                                    M1TB7.Enabled = false;
                                    M1TB8.Enabled = false;
                                    M1TB9.Enabled = false;
                                }
                                else
                                {
                                    if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 1)
                                    {
                                        M1TB1.Enabled = true;
                                        M1TB2.Enabled = false;
                                        M1TB3.Enabled = false;
                                        M1TB4.Enabled = false;
                                        M1TB5.Enabled = false;
                                        M1TB6.Enabled = false;
                                        M1TB7.Enabled = false;
                                        M1TB8.Enabled = false;
                                        M1TB9.Enabled = false;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 1)
                                        {
                                            M1TB1.Enabled = true;
                                            M1TB2.Enabled = false;
                                            M1TB3.Enabled = false;
                                            M1TB4.Enabled = true;
                                            M1TB5.Enabled = false;
                                            M1TB6.Enabled = false;
                                            M1TB7.Enabled = false;
                                            M1TB8.Enabled = false;
                                            M1TB9.Enabled = false;
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 1)
                                            {
                                                M1TB1.Enabled = true;
                                                M1TB2.Enabled = false;
                                                M1TB3.Enabled = false;
                                                M1TB4.Enabled = true;
                                                M1TB5.Enabled = false;
                                                M1TB6.Enabled = false;
                                                M1TB7.Enabled = true;
                                                M1TB8.Enabled = false;
                                                M1TB9.Enabled = false;
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(textBox28.Text) == 0)
                                                {
                                                    M1TB1.Enabled = false;
                                                    M1TB2.Enabled = false;
                                                    M1TB3.Enabled = false;
                                                    M1TB4.Enabled = false;
                                                    M1TB5.Enabled = false;
                                                    M1TB6.Enabled = false;
                                                    M1TB7.Enabled = false;
                                                    M1TB8.Enabled = false;
                                                    M1TB9.Enabled = false;
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(textBox28.Text) >= 4 || Convert.ToInt32(textBox28.Text) <= -1)
                                                    {
                                                        MessageBox.Show("El tamaño maximo de la matriz es de 3x3", "ALERTA");
                                                        textBox28.Focus();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void M1Y_TextChanged(object sender, EventArgs e)
        {
            if (M1Y.Text == "")
            {
                M1Y.Text = "0";
            }
            if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 3)
            {
                M1TB1.Enabled = true;
                M1TB2.Enabled = true;
                M1TB3.Enabled = true;
                M1TB4.Enabled = true;
                M1TB5.Enabled = true;
                M1TB6.Enabled = true;
                M1TB7.Enabled = true;
                M1TB8.Enabled = true;
                M1TB9.Enabled = true;
            }
            else
            {
                if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 3)

                {
                    M1TB1.Enabled = true;
                    M1TB2.Enabled = true;
                    M1TB3.Enabled = true;
                    M1TB4.Enabled = true;
                    M1TB5.Enabled = true;
                    M1TB6.Enabled = true;
                    M1TB7.Enabled = false;
                    M1TB8.Enabled = false;
                    M1TB9.Enabled = false;
                }

                else
                {
                    if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 3)
                    {
                        M1TB1.Enabled = true;
                        M1TB2.Enabled = true;
                        M1TB3.Enabled = true;
                        M1TB4.Enabled = false;
                        M1TB5.Enabled = false;
                        M1TB6.Enabled = false;
                        M1TB7.Enabled = false;
                        M1TB8.Enabled = false;
                        M1TB9.Enabled = false;
                    }
                    else
                    {
                        if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 2)
                        {
                            M1TB1.Enabled = true;
                            M1TB2.Enabled = true;
                            M1TB3.Enabled = false;
                            M1TB4.Enabled = true;
                            M1TB5.Enabled = true;
                            M1TB6.Enabled = false;
                            M1TB7.Enabled = true;
                            M1TB8.Enabled = true;
                            M1TB9.Enabled = false;
                        }
                        else
                        {
                            if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 2)
                            {
                                M1TB1.Enabled = true;
                                M1TB2.Enabled = true;
                                M1TB3.Enabled = false;
                                M1TB4.Enabled = true;
                                M1TB5.Enabled = true;
                                M1TB6.Enabled = false;
                                M1TB7.Enabled = false;
                                M1TB8.Enabled = false;
                                M1TB9.Enabled = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 2)
                                {
                                    M1TB1.Enabled = true;
                                    M1TB2.Enabled = true;
                                    M1TB3.Enabled = false;
                                    M1TB4.Enabled = false;
                                    M1TB5.Enabled = false;
                                    M1TB6.Enabled = false;
                                    M1TB7.Enabled = false;
                                    M1TB8.Enabled = false;
                                    M1TB9.Enabled = false;
                                }
                                else
                                {
                                    if (Convert.ToInt32(M1Y.Text) == 1 && Convert.ToInt32(textBox28.Text) == 1)
                                    {
                                        M1TB1.Enabled = true;
                                        M1TB2.Enabled = false;
                                        M1TB3.Enabled = false;
                                        M1TB4.Enabled = false;
                                        M1TB5.Enabled = false;
                                        M1TB6.Enabled = false;
                                        M1TB7.Enabled = false;
                                        M1TB8.Enabled = false;
                                        M1TB9.Enabled = false;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(M1Y.Text) == 2 && Convert.ToInt32(textBox28.Text) == 1)
                                        {
                                            M1TB1.Enabled = true;
                                            M1TB2.Enabled = false;
                                            M1TB3.Enabled = false;
                                            M1TB4.Enabled = true;
                                            M1TB5.Enabled = false;
                                            M1TB6.Enabled = false;
                                            M1TB7.Enabled = false;
                                            M1TB8.Enabled = false;
                                            M1TB9.Enabled = false;
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(M1Y.Text) == 3 && Convert.ToInt32(textBox28.Text) == 1)
                                            {
                                                M1TB1.Enabled = true;
                                                M1TB2.Enabled = false;
                                                M1TB3.Enabled = false;
                                                M1TB4.Enabled = true;
                                                M1TB5.Enabled = false;
                                                M1TB6.Enabled = false;
                                                M1TB7.Enabled = true;
                                                M1TB8.Enabled = false;
                                                M1TB9.Enabled = false;
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(M1Y.Text) == 0)
                                                {
                                                    M1TB1.Enabled = false;
                                                    M1TB2.Enabled = false;
                                                    M1TB3.Enabled = false;
                                                    M1TB4.Enabled = false;
                                                    M1TB5.Enabled = false;
                                                    M1TB6.Enabled = false;
                                                    M1TB7.Enabled = false;
                                                    M1TB8.Enabled = false;
                                                    M1TB9.Enabled = false;
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(M1Y.Text) >= 4 || Convert.ToInt32(M1Y.Text) <= -1)
                                                    {
                                                        MessageBox.Show("El tamaño maximo de la matriz es de 3x3", "ALERTA");
                                                        M1Y.Focus();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void M2X_TextChanged(object sender, EventArgs e)
        {
            if (M2X.Text == "")
            {
                M2X.Text = "0";
            }
            if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 3)
            {
                M2TB1.Enabled = true;
                M2TB2.Enabled = true;
                M2TB3.Enabled = true;
                M2TB4.Enabled = true;
                M2TB5.Enabled = true;
                M2TB6.Enabled = true;
                M2TB7.Enabled = true;
                M2TB8.Enabled = true;
                MT2TB9.Enabled = true;
            }
            else
            {
                if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 3)

                {
                    M2TB1.Enabled = true;
                    M2TB2.Enabled = true;
                    M2TB3.Enabled = true;
                    M2TB4.Enabled = true;
                    M2TB5.Enabled = true;
                    M2TB6.Enabled = true;
                    M2TB7.Enabled = false;
                    M2TB8.Enabled = false;
                    MT2TB9.Enabled = false;
                }

                else
                {
                    if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 3)
                    {
                        M2TB1.Enabled = true;
                        M2TB2.Enabled = true;
                        M2TB3.Enabled = true;
                        M2TB4.Enabled = false;
                        M2TB5.Enabled = false;
                        M2TB6.Enabled = false;
                        M2TB7.Enabled = false;
                        M2TB8.Enabled = false;
                        MT2TB9.Enabled = false;
                    }
                    else
                    {
                        if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 2)
                        {
                            M2TB1.Enabled = true;
                            M2TB2.Enabled = true;
                            M2TB3.Enabled = false;
                            M2TB4.Enabled = true;
                            M2TB5.Enabled = true;
                            M2TB6.Enabled = false;
                            M2TB7.Enabled = true;
                            M2TB8.Enabled = true;
                            MT2TB9.Enabled = false;
                        }
                        else
                        {
                            if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 2)
                            {
                                M2TB1.Enabled = true;
                                M2TB2.Enabled = true;
                                M2TB3.Enabled = false;
                                M2TB4.Enabled = true;
                                M2TB5.Enabled = true;
                                M2TB6.Enabled = false;
                                M2TB7.Enabled = false;
                                M2TB8.Enabled = false;
                                MT2TB9.Enabled = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 2)
                                {
                                    M2TB1.Enabled = true;
                                    M2TB2.Enabled = true;
                                    M2TB3.Enabled = false;
                                    M2TB4.Enabled = false;
                                    M2TB5.Enabled = false;
                                    M2TB6.Enabled = false;
                                    M2TB7.Enabled = false;
                                    M2TB8.Enabled = false;
                                    MT2TB9.Enabled = false;
                                }
                                else
                                {
                                    if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 1)
                                    {
                                        M2TB1.Enabled = true;
                                        M2TB2.Enabled = false;
                                        M2TB3.Enabled = false;
                                        M2TB4.Enabled = false;
                                        M2TB5.Enabled = false;
                                        M2TB6.Enabled = false;
                                        M2TB7.Enabled = false;
                                        M2TB8.Enabled = false;
                                        MT2TB9.Enabled = false;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 1)
                                        {
                                            M2TB1.Enabled = true;
                                            M2TB2.Enabled = false;
                                            M2TB3.Enabled = false;
                                            M2TB4.Enabled = true;
                                            M2TB5.Enabled = false;
                                            M2TB6.Enabled = false;
                                            M2TB7.Enabled = false;
                                            M2TB8.Enabled = false;
                                            MT2TB9.Enabled = false;
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 1)
                                            {
                                                M2TB1.Enabled = true;
                                                M2TB2.Enabled = false;
                                                M2TB3.Enabled = false;
                                                M2TB4.Enabled = true;
                                                M2TB5.Enabled = false;
                                                M2TB6.Enabled = false;
                                                M2TB7.Enabled = true;
                                                M2TB8.Enabled = false;
                                                MT2TB9.Enabled = false;
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(M2X.Text) == 0)
                                                {
                                                    M2TB1.Enabled = false;
                                                    M2TB2.Enabled = false;
                                                    M2TB3.Enabled = false;
                                                    M2TB4.Enabled = false;
                                                    M2TB5.Enabled = false;
                                                    M2TB6.Enabled = false;
                                                    M2TB7.Enabled = false;
                                                    M2TB8.Enabled = false;
                                                    MT2TB9.Enabled = false;
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(M2X.Text) >= 4 || Convert.ToInt32(M2X.Text) <= -1)
                                                    {
                                                        MessageBox.Show("El tamaño maximo de la matriz es de 3x3", "ALERTA");
                                                        M2X.Focus();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void M2Y_TextChanged(object sender, EventArgs e)
        {
            if (M2Y.Text == "")
            {
                M2Y.Text = "0";
            }
            if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 3)
            {
                M2TB1.Enabled = true;
                M2TB2.Enabled = true;
                M2TB3.Enabled = true;
                M2TB4.Enabled = true;
                M2TB5.Enabled = true;
                M2TB6.Enabled = true;
                M2TB7.Enabled = true;
                M2TB8.Enabled = true;
                MT2TB9.Enabled = true;
            }
            else
            {
                if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 3)

                {
                    M2TB1.Enabled = true;
                    M2TB2.Enabled = true;
                    M2TB3.Enabled = true;
                    M2TB4.Enabled = true;
                    M2TB5.Enabled = true;
                    M2TB6.Enabled = true;
                    M2TB7.Enabled = false;
                    M2TB8.Enabled = false;
                    MT2TB9.Enabled = false;
                }

                else
                {
                    if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 3)
                    {
                        M2TB1.Enabled = true;
                        M2TB2.Enabled = true;
                        M2TB3.Enabled = true;
                        M2TB4.Enabled = false;
                        M2TB5.Enabled = false;
                        M2TB6.Enabled = false;
                        M2TB7.Enabled = false;
                        M2TB8.Enabled = false;
                        MT2TB9.Enabled = false;
                    }
                    else
                    {
                        if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 2)
                        {
                            M2TB1.Enabled = true;
                            M2TB2.Enabled = true;
                            M2TB3.Enabled = false;
                            M2TB4.Enabled = true;
                            M2TB5.Enabled = true;
                            M2TB6.Enabled = false;
                            M2TB7.Enabled = true;
                            M2TB8.Enabled = true;
                            MT2TB9.Enabled = false;
                        }
                        else
                        {
                            if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 2)
                            {
                                M2TB1.Enabled = true;
                                M2TB2.Enabled = true;
                                M2TB3.Enabled = false;
                                M2TB4.Enabled = true;
                                M2TB5.Enabled = true;
                                M2TB6.Enabled = false;
                                M2TB7.Enabled = false;
                                M2TB8.Enabled = false;
                                MT2TB9.Enabled = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 2)
                                {
                                    M2TB1.Enabled = true;
                                    M2TB2.Enabled = true;
                                    M2TB3.Enabled = false;
                                    M2TB4.Enabled = false;
                                    M2TB5.Enabled = false;
                                    M2TB6.Enabled = false;
                                    M2TB7.Enabled = false;
                                    M2TB8.Enabled = false;
                                    MT2TB9.Enabled = false;
                                }
                                else
                                {
                                    if (Convert.ToInt32(M2Y.Text) == 1 && Convert.ToInt32(M2X.Text) == 1)
                                    {
                                        M2TB1.Enabled = true;
                                        M2TB2.Enabled = false;
                                        M2TB3.Enabled = false;
                                        M2TB4.Enabled = false;
                                        M2TB5.Enabled = false;
                                        M2TB6.Enabled = false;
                                        M2TB7.Enabled = false;
                                        M2TB8.Enabled = false;
                                        MT2TB9.Enabled = false;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(M2Y.Text) == 2 && Convert.ToInt32(M2X.Text) == 1)
                                        {
                                            M2TB1.Enabled = true;
                                            M2TB2.Enabled = false;
                                            M2TB3.Enabled = false;
                                            M2TB4.Enabled = true;
                                            M2TB5.Enabled = false;
                                            M2TB6.Enabled = false;
                                            M2TB7.Enabled = false;
                                            M2TB8.Enabled = false;
                                            MT2TB9.Enabled = false;
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(M2Y.Text) == 3 && Convert.ToInt32(M2X.Text) == 1)
                                            {
                                                M2TB1.Enabled = true;
                                                M2TB2.Enabled = false;
                                                M2TB3.Enabled = false;
                                                M2TB4.Enabled = true;
                                                M2TB5.Enabled = false;
                                                M2TB6.Enabled = false;
                                                M2TB7.Enabled = true;
                                                M2TB8.Enabled = false;
                                                MT2TB9.Enabled = false;
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(M2Y.Text) == 0)
                                                {
                                                    M2TB1.Enabled = false;
                                                    M2TB2.Enabled = false;
                                                    M2TB3.Enabled = false;
                                                    M2TB4.Enabled = false;
                                                    M2TB5.Enabled = false;
                                                    M2TB6.Enabled = false;
                                                    M2TB7.Enabled = false;
                                                    M2TB8.Enabled = false;
                                                    MT2TB9.Enabled = false;
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(M2Y.Text) >= 4 || Convert.ToInt32(M2Y.Text) <= -1)
                                                    {
                                                        MessageBox.Show("El tamaño maximo de la matriz es de 3x3", "ALERTA");
                                                        M2Y.Focus();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Calcular_Click(object sender, EventArgs e)
        {
            double r1, r2, r3, r4, r5, r6, r7, r8, r9;
            xm1 = Convert.ToInt32(textBox28.Text);
            ym1 = Convert.ToInt32(M2Y.Text);
            if (radioButton1.Checked == false && radioButton2.Checked == false && radioButton3.Checked == false && radioButton4.Checked == false)
            {
                MessageBox.Show("Selecciona una operación", "ALERTA",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                if (radioButton1.Checked == true)
                {
                    if (Convert.ToInt32(M1Y.Text) == Convert.ToInt32(M2X.Text) && Convert.ToInt32(M1Y.Text) != 0 && Convert.ToInt32(M2Y.Text) != 0 && Convert.ToInt32(M2X.Text) != 0 && Convert.ToInt32(textBox28.Text) != 0)
                    {

                        
                        //Multiplicacion

                        r1 = Convert.ToDouble(M1TB1.Text) * Convert.ToDouble(M2TB1.Text) + Convert.ToDouble(M1TB4.Text) * Convert.ToDouble(M2TB2.Text) + Convert.ToDouble(M1TB7.Text) * Convert.ToDouble(M2TB3.Text);
                        M3TB1.Text = Convert.ToString(r1);
                        r2 = Convert.ToDouble(M1TB1.Text) * Convert.ToDouble(M2TB4.Text) + Convert.ToDouble(M1TB4.Text) * Convert.ToDouble(M2TB5.Text) + Convert.ToDouble(M1TB7.Text) * Convert.ToDouble(M2TB6.Text);
                        M3TB2.Text = Convert.ToString(r2);
                        r3 = Convert.ToDouble(M1TB1.Text) * Convert.ToDouble(M2TB7.Text) + Convert.ToDouble(M1TB4.Text) * Convert.ToDouble(M2TB8.Text) + Convert.ToDouble(M1TB7.Text) * Convert.ToDouble(MT2TB9.Text);
                        M3TB3.Text = Convert.ToString(r3);
                        r4 = Convert.ToDouble(M1TB2.Text) * Convert.ToDouble(M2TB1.Text) + Convert.ToDouble(M1TB5.Text) * Convert.ToDouble(M2TB2.Text) + Convert.ToDouble(M1TB8.Text) * Convert.ToDouble(M2TB3.Text);
                        M3TB4.Text = Convert.ToString(r4);
                        r5 = Convert.ToDouble(M1TB2.Text) * Convert.ToDouble(M2TB4.Text) + Convert.ToDouble(M1TB5.Text) * Convert.ToDouble(M2TB5.Text) + Convert.ToDouble(M1TB8.Text) * Convert.ToDouble(M2TB6.Text);
                        M3TB5.Text = Convert.ToString(r5);
                        r6 = Convert.ToDouble(M1TB2.Text) * Convert.ToDouble(M2TB7.Text) + Convert.ToDouble(M1TB5.Text) * Convert.ToDouble(M2TB8.Text) + Convert.ToDouble(M1TB8.Text) * Convert.ToDouble(MT2TB9.Text);
                        M3TB6.Text = Convert.ToString(r6);
                        r7 = Convert.ToDouble(M1TB3.Text) * Convert.ToDouble(M2TB1.Text) + Convert.ToDouble(M1TB6.Text) * Convert.ToDouble(M2TB2.Text) + Convert.ToDouble(M1TB9.Text) * Convert.ToDouble(M2TB3.Text);
                        M3TB7.Text = Convert.ToString(r7);
                        r8 = Convert.ToDouble(M1TB3.Text) * Convert.ToDouble(M2TB4.Text) + Convert.ToDouble(M1TB6.Text) * Convert.ToDouble(M2TB5.Text) + Convert.ToDouble(M1TB9.Text) * Convert.ToDouble(M2TB6.Text);
                        M3TB8.Text = Convert.ToString(r8);
                        r9 = Convert.ToDouble(M1TB3.Text) * Convert.ToDouble(M2TB7.Text) + Convert.ToDouble(M1TB6.Text) * Convert.ToDouble(M2TB8.Text) + Convert.ToDouble(M1TB9.Text) * Convert.ToDouble(MT2TB9.Text);
                        M3TB9.Text = Convert.ToString(r9);
                    }
                    else
                    {
                        MessageBox.Show("No se puede realizar una operacion con estas matricez", "ALERTA");
                    }
                }
                if (radioButton2.Checked == true)
                {
                    if (Convert.ToInt32(textBox28.Text) == Convert.ToInt32(M2X.Text) && Convert.ToInt32(M2Y.Text) == Convert.ToInt32(M1Y.Text))
                    {
                        r1 = Convert.ToDouble(M1TB1.Text) + Convert.ToDouble(M2TB1.Text);
                        M3TB1.Text = Convert.ToString(r1);
                        r2 = Convert.ToDouble(M1TB2.Text) + Convert.ToDouble(M2TB2.Text);
                        M3TB2.Text = Convert.ToString(r2);
                        r3 = Convert.ToDouble(M1TB3.Text) + Convert.ToDouble(M2TB3.Text);
                        M3TB3.Text = Convert.ToString(r3);
                        r4 = Convert.ToDouble(M1TB4.Text) + Convert.ToDouble(M2TB4.Text);
                        M3TB4.Text = Convert.ToString(r4);
                        r5 = Convert.ToDouble(M1TB5.Text) + Convert.ToDouble(M2TB5.Text);
                        M3TB5.Text = Convert.ToString(r5);
                        r6 = Convert.ToDouble(M1TB6.Text) + Convert.ToDouble(M2TB6.Text);
                        M3TB6.Text = Convert.ToString(r6);
                        r7 = Convert.ToDouble(M1TB7.Text) + Convert.ToDouble(M2TB7.Text);
                        M3TB7.Text = Convert.ToString(r7);
                        r8 = Convert.ToDouble(M1TB8.Text) + Convert.ToDouble(M2TB8.Text);
                        M3TB8.Text = Convert.ToString(r8);
                        r9 = Convert.ToDouble(M1TB9.Text) + Convert.ToDouble(MT2TB9.Text);
                        M3TB9.Text = Convert.ToString(r9);
                    }
                    else
                    {
                        MessageBox.Show("No se puede realizar una operacion con estas matricez", "ALERTA");
                    }
                }
                if (radioButton3.Checked == true)
                {
                    if (Convert.ToInt32(textBox28.Text) == Convert.ToInt32(M2X.Text) && Convert.ToInt32(M2Y.Text) == Convert.ToInt32(M1Y.Text))
                    {
                        r1 = Convert.ToDouble(M1TB1.Text) - Convert.ToDouble(M2TB1.Text);
                        M3TB1.Text = Convert.ToString(r1);
                        r2 = Convert.ToDouble(M1TB2.Text) - Convert.ToDouble(M2TB2.Text);
                        M3TB2.Text = Convert.ToString(r2);
                        r3 = Convert.ToDouble(M1TB3.Text) - Convert.ToDouble(M2TB3.Text);
                        M3TB3.Text = Convert.ToString(r3);
                        r4 = Convert.ToDouble(M1TB4.Text) - Convert.ToDouble(M2TB4.Text);
                        M3TB4.Text = Convert.ToString(r4);
                        r5 = Convert.ToDouble(M1TB5.Text) - Convert.ToDouble(M2TB5.Text);
                        M3TB5.Text = Convert.ToString(r5);
                        r6 = Convert.ToDouble(M1TB6.Text) - Convert.ToDouble(M2TB6.Text);
                        M3TB6.Text = Convert.ToString(r6);
                        r7 = Convert.ToDouble(M1TB7.Text) - Convert.ToDouble(M2TB7.Text);
                        M3TB7.Text = Convert.ToString(r7);
                        r8 = Convert.ToDouble(M1TB8.Text) - Convert.ToDouble(M2TB8.Text);
                        M3TB8.Text = Convert.ToString(r8);
                        r9 = Convert.ToDouble(M1TB9.Text) - Convert.ToDouble(MT2TB9.Text);
                        M3TB9.Text = Convert.ToString(r9);
                    }
                    else
                    {
                        MessageBox.Show("No se puede realizar una operacion con estas matricez", "ALERTA");
                    }
                }
                if (radioButton4.Checked == true)
                {
                    if (Convert.ToInt32(textBox28.Text) == Convert.ToInt32(M2X.Text) && Convert.ToInt32(M2Y.Text) == Convert.ToInt32(M1Y.Text))
                    {
                        r1 = Convert.ToDouble(M1TB1.Text) / Convert.ToDouble(M2TB1.Text);
                        M3TB1.Text = Convert.ToString(r1);
                        r2 = Convert.ToDouble(M1TB2.Text) / Convert.ToDouble(M2TB2.Text);
                        M3TB2.Text = Convert.ToString(r2);
                        r3 = Convert.ToDouble(M1TB3.Text) / Convert.ToDouble(M2TB3.Text);
                        M3TB3.Text = Convert.ToString(r3);
                        r4 = Convert.ToDouble(M1TB4.Text) / Convert.ToDouble(M2TB4.Text);
                        M3TB4.Text = Convert.ToString(r4);
                        r5 = Convert.ToDouble(M1TB5.Text) / Convert.ToDouble(M2TB5.Text);
                        M3TB5.Text = Convert.ToString(r5);
                        r6 = Convert.ToDouble(M1TB6.Text) / Convert.ToDouble(M2TB6.Text);
                        M3TB6.Text = Convert.ToString(r6);
                        r7 = Convert.ToDouble(M1TB7.Text) / Convert.ToDouble(M2TB7.Text);
                        M3TB7.Text = Convert.ToString(r7);
                        r8 = Convert.ToDouble(M1TB8.Text) / Convert.ToDouble(M2TB8.Text);
                        M3TB8.Text = Convert.ToString(r8);
                        r9 = Convert.ToDouble(M1TB9.Text) / Convert.ToDouble(MT2TB9.Text);
                        M3TB9.Text = Convert.ToString(r9);
                    }
                    else
                    {
                        MessageBox.Show("No se puede realizar una operacion con estas matricez", "ALERTA");
                    }
                }
                textBox1.Text = Convert.ToString(xm1);
                textBox2.Text = Convert.ToString(ym1);
            }
            
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            M1TB1.Enabled = false;
            M1TB2.Enabled = false;
            M1TB3.Enabled = false;
            M1TB4.Enabled = false;
            M1TB5.Enabled = false;
            M1TB6.Enabled = false;
            M1TB7.Enabled = false;
            M1TB8.Enabled = false;
            M1TB9.Enabled = false;
            M2TB1.Enabled = false;
            M2TB2.Enabled = false;
            M2TB3.Enabled = false;
            M2TB4.Enabled = false;
            M2TB5.Enabled = false;
            M2TB6.Enabled = false;
            M2TB7.Enabled = false;
            M2TB8.Enabled = false;
            MT2TB9.Enabled = false;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
           
            label7.Text = "X";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
           
            label7.Text = "+";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            label7.Text = "-";
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
           
            label7.Text = "÷";
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void M3TB9_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB8_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB7_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB6_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB5_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB4_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB3_TextChanged(object sender, EventArgs e)
        {

        }

        private void M3TB2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox28_Validating(object sender, CancelEventArgs e)
        {
            

        }
    }
}

﻿
namespace ProyectoAlgebra
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.M1TB1 = new System.Windows.Forms.TextBox();
            this.M1TB4 = new System.Windows.Forms.TextBox();
            this.M1TB7 = new System.Windows.Forms.TextBox();
            this.M1TB2 = new System.Windows.Forms.TextBox();
            this.M1TB5 = new System.Windows.Forms.TextBox();
            this.M1TB8 = new System.Windows.Forms.TextBox();
            this.M1TB3 = new System.Windows.Forms.TextBox();
            this.M1TB6 = new System.Windows.Forms.TextBox();
            this.M1TB9 = new System.Windows.Forms.TextBox();
            this.M2TB1 = new System.Windows.Forms.TextBox();
            this.M2TB2 = new System.Windows.Forms.TextBox();
            this.M2TB3 = new System.Windows.Forms.TextBox();
            this.M2TB4 = new System.Windows.Forms.TextBox();
            this.M2TB5 = new System.Windows.Forms.TextBox();
            this.M2TB6 = new System.Windows.Forms.TextBox();
            this.M2TB7 = new System.Windows.Forms.TextBox();
            this.M2TB8 = new System.Windows.Forms.TextBox();
            this.MT2TB9 = new System.Windows.Forms.TextBox();
            this.M3TB1 = new System.Windows.Forms.TextBox();
            this.M3TB2 = new System.Windows.Forms.TextBox();
            this.M3TB3 = new System.Windows.Forms.TextBox();
            this.M3TB4 = new System.Windows.Forms.TextBox();
            this.M3TB5 = new System.Windows.Forms.TextBox();
            this.M3TB6 = new System.Windows.Forms.TextBox();
            this.M3TB7 = new System.Windows.Forms.TextBox();
            this.M3TB8 = new System.Windows.Forms.TextBox();
            this.M3TB9 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.M1Y = new System.Windows.Forms.TextBox();
            this.M2X = new System.Windows.Forms.TextBox();
            this.M2Y = new System.Windows.Forms.TextBox();
            this.Calcular = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Javanese Text", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(485, 66);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operacion de Matrices Hasta 3X3";
            // 
            // M1TB1
            // 
            this.M1TB1.Location = new System.Drawing.Point(26, 199);
            this.M1TB1.Name = "M1TB1";
            this.M1TB1.Size = new System.Drawing.Size(100, 20);
            this.M1TB1.TabIndex = 2;
            this.M1TB1.Text = "0";
            // 
            // M1TB4
            // 
            this.M1TB4.Location = new System.Drawing.Point(143, 199);
            this.M1TB4.Name = "M1TB4";
            this.M1TB4.Size = new System.Drawing.Size(100, 20);
            this.M1TB4.TabIndex = 3;
            this.M1TB4.Text = "0";
            // 
            // M1TB7
            // 
            this.M1TB7.Location = new System.Drawing.Point(261, 199);
            this.M1TB7.Name = "M1TB7";
            this.M1TB7.Size = new System.Drawing.Size(100, 20);
            this.M1TB7.TabIndex = 4;
            this.M1TB7.Text = "0";
            // 
            // M1TB2
            // 
            this.M1TB2.Location = new System.Drawing.Point(26, 243);
            this.M1TB2.Name = "M1TB2";
            this.M1TB2.Size = new System.Drawing.Size(100, 20);
            this.M1TB2.TabIndex = 5;
            this.M1TB2.Text = "0";
            // 
            // M1TB5
            // 
            this.M1TB5.Location = new System.Drawing.Point(143, 243);
            this.M1TB5.Name = "M1TB5";
            this.M1TB5.Size = new System.Drawing.Size(100, 20);
            this.M1TB5.TabIndex = 6;
            this.M1TB5.Text = "0";
            // 
            // M1TB8
            // 
            this.M1TB8.Location = new System.Drawing.Point(261, 243);
            this.M1TB8.Name = "M1TB8";
            this.M1TB8.Size = new System.Drawing.Size(100, 20);
            this.M1TB8.TabIndex = 7;
            this.M1TB8.Text = "0";
            // 
            // M1TB3
            // 
            this.M1TB3.Location = new System.Drawing.Point(26, 293);
            this.M1TB3.Name = "M1TB3";
            this.M1TB3.Size = new System.Drawing.Size(100, 20);
            this.M1TB3.TabIndex = 8;
            this.M1TB3.Text = "0";
            // 
            // M1TB6
            // 
            this.M1TB6.Location = new System.Drawing.Point(143, 293);
            this.M1TB6.Name = "M1TB6";
            this.M1TB6.Size = new System.Drawing.Size(100, 20);
            this.M1TB6.TabIndex = 9;
            this.M1TB6.Text = "0";
            // 
            // M1TB9
            // 
            this.M1TB9.Location = new System.Drawing.Point(261, 293);
            this.M1TB9.Name = "M1TB9";
            this.M1TB9.Size = new System.Drawing.Size(100, 20);
            this.M1TB9.TabIndex = 10;
            this.M1TB9.Text = "0";
            // 
            // M2TB1
            // 
            this.M2TB1.Location = new System.Drawing.Point(411, 199);
            this.M2TB1.Name = "M2TB1";
            this.M2TB1.Size = new System.Drawing.Size(100, 20);
            this.M2TB1.TabIndex = 13;
            this.M2TB1.Text = "0";
            // 
            // M2TB2
            // 
            this.M2TB2.Location = new System.Drawing.Point(411, 243);
            this.M2TB2.Name = "M2TB2";
            this.M2TB2.Size = new System.Drawing.Size(100, 20);
            this.M2TB2.TabIndex = 16;
            this.M2TB2.Text = "0";
            // 
            // M2TB3
            // 
            this.M2TB3.Location = new System.Drawing.Point(411, 293);
            this.M2TB3.Name = "M2TB3";
            this.M2TB3.Size = new System.Drawing.Size(100, 20);
            this.M2TB3.TabIndex = 19;
            this.M2TB3.Text = "0";
            // 
            // M2TB4
            // 
            this.M2TB4.Location = new System.Drawing.Point(528, 199);
            this.M2TB4.Name = "M2TB4";
            this.M2TB4.Size = new System.Drawing.Size(100, 20);
            this.M2TB4.TabIndex = 14;
            this.M2TB4.Text = "0";
            // 
            // M2TB5
            // 
            this.M2TB5.Location = new System.Drawing.Point(528, 243);
            this.M2TB5.Name = "M2TB5";
            this.M2TB5.Size = new System.Drawing.Size(100, 20);
            this.M2TB5.TabIndex = 17;
            this.M2TB5.Text = "0";
            // 
            // M2TB6
            // 
            this.M2TB6.Location = new System.Drawing.Point(528, 293);
            this.M2TB6.Name = "M2TB6";
            this.M2TB6.Size = new System.Drawing.Size(100, 20);
            this.M2TB6.TabIndex = 20;
            this.M2TB6.Text = "0";
            // 
            // M2TB7
            // 
            this.M2TB7.Location = new System.Drawing.Point(646, 199);
            this.M2TB7.Name = "M2TB7";
            this.M2TB7.Size = new System.Drawing.Size(100, 20);
            this.M2TB7.TabIndex = 15;
            this.M2TB7.Text = "0";
            // 
            // M2TB8
            // 
            this.M2TB8.Location = new System.Drawing.Point(646, 243);
            this.M2TB8.Name = "M2TB8";
            this.M2TB8.Size = new System.Drawing.Size(100, 20);
            this.M2TB8.TabIndex = 18;
            this.M2TB8.Text = "0";
            // 
            // MT2TB9
            // 
            this.MT2TB9.Location = new System.Drawing.Point(646, 293);
            this.MT2TB9.Name = "MT2TB9";
            this.MT2TB9.Size = new System.Drawing.Size(100, 20);
            this.MT2TB9.TabIndex = 21;
            this.MT2TB9.Text = "0";
            // 
            // M3TB1
            // 
            this.M3TB1.Location = new System.Drawing.Point(209, 405);
            this.M3TB1.Name = "M3TB1";
            this.M3TB1.ReadOnly = true;
            this.M3TB1.Size = new System.Drawing.Size(100, 20);
            this.M3TB1.TabIndex = 29;
            this.M3TB1.TextChanged += new System.EventHandler(this.M3TB1_TextChanged);
            // 
            // M3TB2
            // 
            this.M3TB2.Location = new System.Drawing.Point(209, 449);
            this.M3TB2.Name = "M3TB2";
            this.M3TB2.ReadOnly = true;
            this.M3TB2.Size = new System.Drawing.Size(100, 20);
            this.M3TB2.TabIndex = 32;
            this.M3TB2.TextChanged += new System.EventHandler(this.M3TB2_TextChanged);
            // 
            // M3TB3
            // 
            this.M3TB3.Location = new System.Drawing.Point(209, 499);
            this.M3TB3.Name = "M3TB3";
            this.M3TB3.ReadOnly = true;
            this.M3TB3.Size = new System.Drawing.Size(100, 20);
            this.M3TB3.TabIndex = 35;
            this.M3TB3.TextChanged += new System.EventHandler(this.M3TB3_TextChanged);
            // 
            // M3TB4
            // 
            this.M3TB4.Location = new System.Drawing.Point(326, 405);
            this.M3TB4.Name = "M3TB4";
            this.M3TB4.ReadOnly = true;
            this.M3TB4.Size = new System.Drawing.Size(100, 20);
            this.M3TB4.TabIndex = 30;
            this.M3TB4.TextChanged += new System.EventHandler(this.M3TB4_TextChanged);
            // 
            // M3TB5
            // 
            this.M3TB5.Location = new System.Drawing.Point(326, 449);
            this.M3TB5.Name = "M3TB5";
            this.M3TB5.ReadOnly = true;
            this.M3TB5.Size = new System.Drawing.Size(100, 20);
            this.M3TB5.TabIndex = 33;
            this.M3TB5.TextChanged += new System.EventHandler(this.M3TB5_TextChanged);
            // 
            // M3TB6
            // 
            this.M3TB6.Location = new System.Drawing.Point(326, 499);
            this.M3TB6.Name = "M3TB6";
            this.M3TB6.ReadOnly = true;
            this.M3TB6.Size = new System.Drawing.Size(100, 20);
            this.M3TB6.TabIndex = 36;
            this.M3TB6.TextChanged += new System.EventHandler(this.M3TB6_TextChanged);
            // 
            // M3TB7
            // 
            this.M3TB7.Location = new System.Drawing.Point(444, 405);
            this.M3TB7.Name = "M3TB7";
            this.M3TB7.ReadOnly = true;
            this.M3TB7.Size = new System.Drawing.Size(100, 20);
            this.M3TB7.TabIndex = 31;
            this.M3TB7.TabStop = false;
            this.M3TB7.TextChanged += new System.EventHandler(this.M3TB7_TextChanged);
            // 
            // M3TB8
            // 
            this.M3TB8.Location = new System.Drawing.Point(444, 449);
            this.M3TB8.Name = "M3TB8";
            this.M3TB8.ReadOnly = true;
            this.M3TB8.Size = new System.Drawing.Size(100, 20);
            this.M3TB8.TabIndex = 34;
            this.M3TB8.TextChanged += new System.EventHandler(this.M3TB8_TextChanged);
            // 
            // M3TB9
            // 
            this.M3TB9.Location = new System.Drawing.Point(444, 499);
            this.M3TB9.Name = "M3TB9";
            this.M3TB9.ReadOnly = true;
            this.M3TB9.Size = new System.Drawing.Size(100, 20);
            this.M3TB9.TabIndex = 37;
            this.M3TB9.TextChanged += new System.EventHandler(this.M3TB9_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(139, 449);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "=";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(103, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "X";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(61, 152);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(37, 20);
            this.textBox28.TabIndex = 0;
            this.textBox28.Text = "0";
            this.textBox28.TextChanged += new System.EventHandler(this.textBox28_TextChanged);
            this.textBox28.Validating += new System.ComponentModel.CancelEventHandler(this.textBox28_Validating);
            // 
            // M1Y
            // 
            this.M1Y.Location = new System.Drawing.Point(132, 152);
            this.M1Y.Name = "M1Y";
            this.M1Y.Size = new System.Drawing.Size(37, 20);
            this.M1Y.TabIndex = 1;
            this.M1Y.Text = "0";
            this.M1Y.TextChanged += new System.EventHandler(this.M1Y_TextChanged);
            // 
            // M2X
            // 
            this.M2X.Location = new System.Drawing.Point(583, 152);
            this.M2X.Name = "M2X";
            this.M2X.Size = new System.Drawing.Size(37, 20);
            this.M2X.TabIndex = 11;
            this.M2X.Text = "0";
            this.M2X.TextChanged += new System.EventHandler(this.M2X_TextChanged);
            // 
            // M2Y
            // 
            this.M2Y.Location = new System.Drawing.Point(652, 152);
            this.M2Y.Name = "M2Y";
            this.M2Y.Size = new System.Drawing.Size(37, 20);
            this.M2Y.TabIndex = 12;
            this.M2Y.Text = "0";
            this.M2Y.TextChanged += new System.EventHandler(this.M2Y_TextChanged);
            // 
            // Calcular
            // 
            this.Calcular.Location = new System.Drawing.Point(635, 437);
            this.Calcular.Name = "Calcular";
            this.Calcular.Size = new System.Drawing.Size(75, 52);
            this.Calcular.TabIndex = 22;
            this.Calcular.Text = "Calcular";
            this.Calcular.UseVisualStyleBackColor = true;
            this.Calcular.Click += new System.EventHandler(this.Calcular_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(624, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 2;
            this.label4.Text = "X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "Tamaño Matriz 1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(554, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tamaño Matriz 2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(373, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 24);
            this.label7.TabIndex = 2;
            this.label7.Text = "X";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(251, 78);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(89, 17);
            this.radioButton1.TabIndex = 38;
            this.radioButton1.Text = "Multiplicacion";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(346, 78);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(52, 17);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.Text = "Suma";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(404, 78);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(53, 17);
            this.radioButton3.TabIndex = 40;
            this.radioButton3.Text = "Resta";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(463, 78);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(60, 17);
            this.radioButton4.TabIndex = 41;
            this.radioButton4.Text = "Divisón";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(192, 379);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(33, 20);
            this.textBox1.TabIndex = 42;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(261, 379);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(33, 20);
            this.textBox2.TabIndex = 42;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(60, 342);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(214, 23);
            this.label8.TabIndex = 5;
            this.label8.Text = "Tamaño Matriz Resultante";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(231, 376);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 24);
            this.label9.TabIndex = 2;
            this.label9.Text = "X";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 581);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Calcular);
            this.Controls.Add(this.M2Y);
            this.Controls.Add(this.M2X);
            this.Controls.Add(this.M1Y);
            this.Controls.Add(this.textBox28);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.M3TB9);
            this.Controls.Add(this.MT2TB9);
            this.Controls.Add(this.M1TB9);
            this.Controls.Add(this.M3TB8);
            this.Controls.Add(this.M2TB8);
            this.Controls.Add(this.M3TB7);
            this.Controls.Add(this.M2TB7);
            this.Controls.Add(this.M1TB8);
            this.Controls.Add(this.M3TB6);
            this.Controls.Add(this.M2TB6);
            this.Controls.Add(this.M1TB7);
            this.Controls.Add(this.M3TB5);
            this.Controls.Add(this.M2TB5);
            this.Controls.Add(this.M1TB6);
            this.Controls.Add(this.M3TB4);
            this.Controls.Add(this.M2TB4);
            this.Controls.Add(this.M1TB5);
            this.Controls.Add(this.M3TB3);
            this.Controls.Add(this.M2TB3);
            this.Controls.Add(this.M1TB4);
            this.Controls.Add(this.M3TB2);
            this.Controls.Add(this.M2TB2);
            this.Controls.Add(this.M3TB1);
            this.Controls.Add(this.M1TB3);
            this.Controls.Add(this.M2TB1);
            this.Controls.Add(this.M1TB2);
            this.Controls.Add(this.M1TB1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox M1TB1;
        private System.Windows.Forms.TextBox M1TB4;
        private System.Windows.Forms.TextBox M1TB7;
        private System.Windows.Forms.TextBox M1TB2;
        private System.Windows.Forms.TextBox M1TB5;
        private System.Windows.Forms.TextBox M1TB8;
        private System.Windows.Forms.TextBox M1TB3;
        private System.Windows.Forms.TextBox M1TB6;
        private System.Windows.Forms.TextBox M1TB9;
        private System.Windows.Forms.TextBox M2TB1;
        private System.Windows.Forms.TextBox M2TB2;
        private System.Windows.Forms.TextBox M2TB3;
        private System.Windows.Forms.TextBox M2TB4;
        private System.Windows.Forms.TextBox M2TB5;
        private System.Windows.Forms.TextBox M2TB6;
        private System.Windows.Forms.TextBox M2TB7;
        private System.Windows.Forms.TextBox M2TB8;
        private System.Windows.Forms.TextBox MT2TB9;
        private System.Windows.Forms.TextBox M3TB1;
        private System.Windows.Forms.TextBox M3TB2;
        private System.Windows.Forms.TextBox M3TB3;
        private System.Windows.Forms.TextBox M3TB4;
        private System.Windows.Forms.TextBox M3TB5;
        private System.Windows.Forms.TextBox M3TB6;
        private System.Windows.Forms.TextBox M3TB7;
        private System.Windows.Forms.TextBox M3TB8;
        private System.Windows.Forms.TextBox M3TB9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox M1Y;
        private System.Windows.Forms.TextBox M2X;
        private System.Windows.Forms.TextBox M2Y;
        private System.Windows.Forms.Button Calcular;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}


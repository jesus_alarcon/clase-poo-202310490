﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Herencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;
        public void metodo1()
        {
            Console.WriteLine("Este es wl metodo de la clase herencia");
        }

        private void metodo2()
        {
            Console.WriteLine("Este metodo no es accesible fuera de la clase herencia");
        }
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de la clase Herencia, tiene protected");
        }
        public void AccesoPrivate()
        {
            metodo2();
        }

    }
    class Hijo: Herencia
    {
        public void accesoProtected()
        {
            metodo3();
        }
    }
}

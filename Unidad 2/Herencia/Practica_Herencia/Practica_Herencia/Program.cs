﻿using System;

namespace Practica_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creación del objeto a partir de una clase
            Hijo obj = new Hijo();
            //Implementación de metodos a partir de un objeto
            obj.metodo1();
            obj.accesoProtected();

            //Creación de un objeto con propiedades de la clase Herencia
            Herencia obj2 = new Herencia();
            //Implementación de un método con un objeto
            obj2.AccesoPrivate();
        }
    }
}

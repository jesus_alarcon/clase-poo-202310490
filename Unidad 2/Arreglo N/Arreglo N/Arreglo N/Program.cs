﻿using System;

namespace Arreglo_N
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            int a = 0;
            Console.WriteLine("Ingresa la cantidad de datos para almacenar en el arreglo");
            a = int.Parse(Console.ReadLine());


            int[] n = new int[a];
            int[] pares = new int[a];
            {
                for (int i = 0; i < n.Length; i++)
                {
                    Console.WriteLine("Ingresa un número");
                    int num = int.Parse(Console.ReadLine());
                    n[i] = num;
                    if (num % 2 == 0)
                    {
                        pares[i] = num;
                    }




                }
            }
            Console.WriteLine("Los números pares del arreglo son... ");
            foreach (int var in pares)
            {
                Console.WriteLine(var);
            }
            foreach (int val in n)
            {
                suma += val;
            }
            Console.WriteLine("La suma total del arreglo es ...");
            Console.WriteLine(suma);

        }
    }
}

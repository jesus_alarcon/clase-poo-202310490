﻿using System;

namespace Sobrecarga_Jueves_20_Abril
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();
            int resultado = obj.Suma(5,9);
            Console.WriteLine("resultado método 1: ",resultado);
            double resultado2 = obj.Suma(10.5555, 18.5555, 26.5555);
            Console.WriteLine("resultado método 2: ",resultado2 );
            float resultado3 = obj.Suma(1.5f,2.5f,3.5f);
            Console.WriteLine("Resultado método 3: ",resultado3);
        }
    }
}

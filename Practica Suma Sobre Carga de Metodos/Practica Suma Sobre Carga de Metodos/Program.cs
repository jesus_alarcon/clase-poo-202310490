﻿using System;

namespace Practica_Suma_Sobre_Carga_de_Metodos
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();
            int resultado = obj.Suma(5, 9);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            double resultado2 = obj.Suma(5.888854, 9.748465465, 3.14323254);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            string resultado3 = obj.Suma("1", "8");
            float resultado4 = obj.Suma(3.9f, 8.9f, 8.9f);
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            Console.WriteLine("Resultado metodo 4: {0}", resultado4);
        }
    }
}

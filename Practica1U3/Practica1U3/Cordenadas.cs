﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1U3
{

    public partial class Cordenadas : Form
    {
        Graphics vector;
        int xcentro;
        int ycentro;
        public Cordenadas()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.Enabled = false;
        }

        public void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            int xcentro = pictureBox1.Width / 2;
            int ycentro = pictureBox1.Height / 2;
            Pen lapizN = new Pen(Color.Black, 2);
            
            e.Graphics.TranslateTransform(xcentro, ycentro);
            e.Graphics.ScaleTransform(1, -1);
            e.Graphics.DrawLine(lapizN, xcentro * -1, 0, xcentro * 2, 0);
            e.Graphics.DrawLine(lapizN,0,ycentro,0,ycentro*-1);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Pen lapizR = new Pen(Color.Red, 2);
            
            vector = pictureBox1.CreateGraphics();
            vector.DrawLine(lapizR, pictureBox1.Width/4,pictureBox1.Height/4 , pictureBox1.Width / 4, Convert.ToInt32(textBox2.Text));
            vector.DrawLine(lapizR, pictureBox1.Width / 4, pictureBox1.Height / 4, Convert.ToInt32(textBox1.Text), pictureBox1.Height / 4);
            vector.DrawLine(lapizR, pictureBox1.Width / 4, pictureBox1.Height / 4, Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox1.Text)+3);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            vector = pictureBox1.CreateGraphics();
            vector.Clear(BackColor);
            int xcentro = pictureBox1.Width / 2;
            int ycentro = pictureBox1.Height / 2;
            Pen lapizN = new Pen(Color.Black, 2);
            vector.DrawLine(lapizN, xcentro * -1, 0, xcentro * 2, 0);
            vector.DrawLine(lapizN, 0, ycentro, 0, ycentro * -1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }

}

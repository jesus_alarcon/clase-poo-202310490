﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1U3
{
    public partial class Menu : Form
    {
        internal static bool acceso;
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cordenadas obj = new Cordenadas();
            obj.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Desea ingresar datos?", "Responda", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                aerolineahabilitar obj = new aerolineahabilitar();
                obj.Activate();
                obj.Show();
                acceso = true;
            }
            else
            {
                aerolineahabilitar obj = new aerolineahabilitar();
                obj.Show();
            }

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            monitoreoviajes obj = new monitoreoviajes();
            obj.Show();
        }
    }
}

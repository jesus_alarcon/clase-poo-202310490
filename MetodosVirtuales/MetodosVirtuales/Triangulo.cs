﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetodosVirtuales
{
    class Triangulo
    {
        public virtual void area(double b,double h)
        {
            double resultado = (b * h) / 2;
            
            Console.WriteLine("El area del Triangulo\nEl area es: {0}", resultado);
        }
    }
}

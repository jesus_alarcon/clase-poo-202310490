﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 {0}",atributo1);
        }
        private void metodo2()
        {

        }
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 {0}", atributo3);
        }
    }
    class Heredado:Herencia
    {
        public void atributos()
        {
            atributo1 = 1;
            atributo3 = 5;
            metodo1();
            
        }
        public void accesometodo3()
        {
            metodo3();
        }
    }
}
